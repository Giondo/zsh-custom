FROM rockylinux:9

LABEL maintainer="Giondo <giondog@virtualinfra.online>"
LABEL last_changed="2022-11-22"
LABEL build_version="VirtualInfra.online version:- ${VERSION} Build-date:- ${BUILD_DATE}"

COPY binaries/gcloudsdk.repo /etc/yum.repos.d/google-cloud-sdk.repo

# Install all dependencies
RUN dnf update -y \
  && dnf install -y  git python39 zsh google-cloud-cli \
  && dnf -y clean all \
  && mkdir /binaries

COPY binaries /binaries

# RUN ls / && ls /binaries && ls /binaries/aws/
RUN /binaries/aws/install
RUN /usr/local/bin/aws --version
# RUN source /binaries/zsh.linux
# RUN source /binaries/antigen/antigen.zsh
